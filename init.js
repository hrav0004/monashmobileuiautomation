const APP_CENTER = require('./configs/app_center_config');
const COMMAND_LINE= getCommandlineString()
const PLATFORM=getPlatform()
const DEVICE=getDeviceName()
//const APP_URL=uploadApp()


module.exports= {
    commandline:getCommandlineString(),
    tags:getTags(),
    app:downloadApp(),
    testrun:setTestRunData(),
    DEVICE:getDeviceName(),
    OS_VERSION:getOSVersion(),
    PLATFORM,
    //APP_URL
}

function getCommandlineString()
{
    return process.argv.slice()
}

function setTestRunData()
{

}
function downloadApp()
{
    console.log("DOWNLOADING APP")
    // get the api response from app center with the download url
        var exec = require('child_process').execSync;
        if(PLATFORM=="ANDROID")
        {
        var output= exec('curl -X GET "https://api.appcenter.ms/v0.1/sdk/apps/"'+APP_CENTER.APP_CENTER_ANDROID_APP_SECRET+'"/releases/private/latest" -H  "accept: application/json" -H  "X-API-Token: '+APP_CENTER.APP_CENTER_API_TOKEN+'"'); 
        output=JSON.parse(output)
        console.log("DOWNLOAD URL : "+output["download_url"])
        var download_app= exec('curl "'+output["download_url"]+'" --output ./apps/android/android.apk') //download the android app
        }
        else if (PLATFORM=="IOS")
        {
            var output= exec('curl -X GET "https://api.appcenter.ms/v0.1/sdk/apps/"'+APP_CENTER.APP_CENTER_IOS_APP_SECRET+'"/releases/private/latest" -H  "accept: application/json" -H  "X-API-Token: '+APP_CENTER.APP_CENTER_API_TOKEN+'"'); 
            output=JSON.parse(output)
            var download_app= exec('curl "'+output["download_url"]+'" --output ./apps/ios/ios.ipa') //download the ios app
        }
}

function getTags()
{}

function getPlatform()
{
   
    for(param of process.argv.slice())
    {
        if(param.toString().includes("PLATFORM"))
        {
            return param.split("=")[1].toString()
           // return PLATFORM
        }
    }
   
}

function getDeviceName()
{
    for(param of process.argv.slice())
    {
        if(param.toString().includes("DEVICE"))
        {
            return param.split("=")[1].toString()
        }
    }
}

function getOSVersion()
{
    for(param of process.argv.slice())
    {
        if(param.toString().includes("OS"))
        {
            return param.split("=")[1].toString()
        }
    }
}

