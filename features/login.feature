Feature: The Internet Guinea Pig Website

  Scenario Outline: As a user, I can log into the secure area


    When I am on the login screen
   
    Examples:
      | username | password             | message                        |
      | tomsmith | SuperSecretPassword! | You logged into a secure area! |
   