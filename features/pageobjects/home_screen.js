
module.exports= {
    get acceptAndSignin_Button()
    {
        return $('//android.widget.Button')
    },
    get monashSafe_Link()
    {
        return $('[]')
    },
    get contactUs_Link()
    {
        return $('[]')
    },
    get usernameTextbox()
    {
        return $('//*[id="okta-signin-username"]')
    },
    get passwordTextbox()
    {
        return $('#okta-signin-password')
    },
    get signInButton()
    {
        return $('#okta-signin-submit')
    }

}